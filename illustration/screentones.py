"""
Replace greyscales in a PNG image with corresponding pattern from a set of screentones provided as PNG images

    Stages of the pipeline:

    1.  load PNG
    2.  if it's not already greyscale, convert it
    3.  load screentones, and sort them by average value
    4.  add all-black and all-white screentones if there isn't a black or white tone in the list
    5.  posterise the source image, each pixel to the nearest of the average screentone values vs
    6.  make separate mask layers of pixels == v, for each value v in vs
    7.  multiply mask layers by the corresponding (tiled) screen tone patterns
    8.  add up the mask layers to get a final image

    Usage:
    screentones [options] INPUT.png [tone1.png, ...]

    Options:
    --out OUTPUT.png  [default out-$INPUT.png]
    --debug-grey FILENAME.png   dump the intermediate image after greyscaling
    --debug-poster FILENAME.png dump the intermediate image after posterising
    --debug-masks FILENAME.png  dump image masks as FILENAME0.png, FILENAME1.png etc.
    --no-black  don't add pure black to the list of grey levels
    --no-white  don't add pure white to the list of grey levels

"""
from argparse import ArgumentParser
import numpy as np
import png


class PNG:
    def __init__(self, info, rows):
        self.info = info.copy()
        self.pixels = np.array(list(rows))

    @classmethod
    def from_file(cls, file):
        h, w, rows, info = png.Reader(file).read()
        return cls(info, rows)

    @classmethod
    def solid(cls, value, width=1, height=1):
        return cls(dict(size=(width, height), greyscale=True, alpha=False, planes=1, bitdepth=8),
                   [[value] * width] * height)

    @classmethod
    def from_masks(cls, mask_list):
        info = mask_list[0].info
        rows = sum(mask.pixels for mask in mask_list)
        return cls(info, rows)

    @property
    def is_greyscale(self):
        return self.info.get("greyscale")

    @property
    def has_alpha(self):
        return self.info.get("alpha")

    def as_greyscale(self):
        if self.is_greyscale:
            return self
        skip = self.info["planes"]
        red = self.pixels[:, 0::skip]
        green = self.pixels[:, 1::skip]
        blue = self.pixels[:, 2::skip]
        info = dict(self.info, alpha=False, greyscale=True, planes=1)
        rows = np.uint8(np.round((red * 3.0 + green * 6.0 + blue * 1.0) * 0.1, 0))
        return PNG(info, rows)

    def write_to(self, filename):
        width, height = self.info["size"]
        writer = png.Writer(width=width, height=height, greyscale=self.is_greyscale)
        with open(filename, "wb") as f:
            writer.write(f, self.pixels)

    @property
    def mean(self, plane=0):
        skip = self.info["planes"]
        return np.mean(self.pixels[:, plane::skip])

    def posterise(self, values):
        values = sorted(values)     # they have to be sorted and indexable
        cutoff_hi = 0
        last = len(values) - 1
        for i, val in enumerate(values):
            cutoff_low = cutoff_hi  # previous high cutoff, no overlap
            if i < last:
                higher = values[i + 1]
                cutoff_hi = (val + higher) // 2
            else:
                cutoff_hi = 256
            # which pixels are in the bracket between lower and upper cutoff value?
            # noinspection PyUnresolvedReferences
            included = np.logical_and(cutoff_low <= self.pixels, self.pixels < cutoff_hi)
            self.pixels[included] = val


class ToneSet:
    def __init__(self, filenames: list, add_black=True, add_white=True):
        tone_list = []
        for filename in filenames:
            tone = PNG.from_file(filename).as_greyscale()
            tone.value = int(round(tone.mean, 0))
            tone_list.append(tone)
        tone_list.sort(key=lambda t: t.value)
        if (not tone_list or tone_list[0].value > 0) and add_black:
            black = PNG.solid(0)
            black.value = 0
            tone_list.insert(0, black)
        if (not tone_list or tone_list[-1].value < 255) and add_white:
            white = PNG.solid(255)
            white.value = 255
            tone_list.append(white)
        self.tone_list = tone_list

    @property
    def values(self):
        return [t.value for t in self.tone_list]

    def make_masks(self, image: PNG):
        height, width = image.pixels.shape
        masks = []
        for tone in self.tone_list:
            th, tw = tone.pixels.shape
            multx = width // tw + (1 if width % tw else 0)
            multy = height // th + (1 if height % th else 0)
            full_size = np.tile(tone.pixels, (multy, multx))[:height, :width]  # pattern tiled to size of image
            index = image.pixels != tone.value  # where is the source image NOT shaded with this value?
            full_size[index] = 0    # mask out that part of pattern
            masks.append(PNG(image.info, full_size))
        return masks


def main():
    ap = ArgumentParser()
    add = ap.add_argument
    add("input", help="input PNG filename")
    add("tones", nargs="*", help="one or more screentone PNGs")
    add("--out", default=None, help="output PNG filename (default is 'out-' + input name")
    add("--debug-grey", default=None, help="filename to save intermediate greyscale image")
    add("--debug-poster", default=None, help="filename to save intermediate posterised image")
    add("--debug-masks", default=None, help="filename prefix to save intermediate masks")
    add("--no-black", default=False, action="store_true", help="don't add pure black tone")
    add("--no-white", default=False, action="store_true", help="don't add pure white tone")
    args = ap.parse_args()
    if not args.out:
        path_parts = args.input.split("/")
        path_parts[-1] = "out-" + path_parts[-1]
        args.out = "/".join(path_parts)
    input_png = PNG.from_file(args.input)
    grey_png = input_png.as_greyscale()
    if args.debug_grey and not input_png.is_greyscale:
        print("Greyscaled", args.debug_grey)
        grey_png.write_to(args.debug_grey)
    tone_set = ToneSet(args.tones, add_black=not args.no_black, add_white=not args.no_white)
    grey_png.posterise(tone_set.values)
    if args.debug_poster:
        print("Posterised", args.debug_poster, "using values", tone_set.values)
        grey_png.write_to(args.debug_poster)
    masks = tone_set.make_masks(grey_png)
    if args.debug_masks:
        for i, mask in enumerate(masks):
            mask.write_to("{}{}.png".format(args.debug_masks, i))
    out_png = PNG.from_masks(masks)
    out_png.write_to(args.out)

if __name__ == '__main__':
    main()
