#!/usr/bin/env python
from setuptools import setup, find_packages

setup(
    name='Writing-Tool-Scripts',
    version='2.1.3',
    description='Scripts to help produce eBooks using LyX and Calibre',
    author='Peter Harris',
    author_email='pete.alex.harris@gmail.com',
    url='https://bitbucket.org/ScavengerEthic/writing-tool-scripts',
    packages=find_packages(),
    install_requires=['numpy', 'pypng', 'lxml'],
    extras_require={'test': 'pytest', 'plot': 'bokeh'},
    entry_points={
        'console_scripts': [
            'fixnesting = fixers.fixnesting:main',
            'screentones = illustration.screentones:main',
            'lyx_to_md = converters.lyx_to_md:main',
            'corpusdiff = analysis.corpusdiff:main',
        ]
        },
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Programming Language :: Python :: 3',
        ],
    license='MIT',
    )
