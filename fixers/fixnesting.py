#!/usr/bin/env python
"""
  Fix LyX export errors to do with nesting of XHTML

  1. where &ldquo; appears before paragraph it should be at the front of.
  2. where <a id="magicparlabel-XXX"></a> appears in a <blockquote> where epubcheck doesn't like it

  fixnesting.py [--prefix=PREFIX] filename.xhtml...
"""
from __future__ import print_function, unicode_literals
import argparse
import re
try:
    from lxml import html
except ImportError:
    html = None


def fix_ldquo(xml):
    return re.sub(
        r'&ldquo;(<div class="[a-zA-Z0-9]+">)',  # left double quote just before div
        r"\1&ldquo;",  # re-order
        xml)


def fix_a_in_blockquote(xml):
    pattern = re.compile(
        r"""
        (<blockquote \s* class="[a-z]+">)           # <blockquote> followed by...
        <a \s* id=['"]magicparlabel-[0-9]+["'] \s*   # <a> tag with magicparlabel id, in any quotes
        (/>|</a>)                               # self-closing or empty with closing </a>
        """,
        re.X
    )
    count = len(list(pattern.findall(xml)))
    print("blockquote fixes:", count)
    return pattern.sub(r"\1", xml)  # only keep the <blockquote>, omit the <a>


def fix_div_chapter_label(xml):
    pattern = re.compile(
        r"""
        <div \s* class="chapter_label">
        ([a-zA-Z 0-9]*)             # chapter number
        </div>
        """,
        re.X
    )
    count = len(list(pattern.findall(xml)))
    print("div.chapter_label fixes:", count)
    return pattern.sub(r'<span class="chapter_label">\1</span>', xml)  # it must be a <span>


def fix_endnotes(xml):
    tree = html.fromstring(xml.encode('utf-8'))
    notes = tree.xpath("//div[contains(@class,'foot')]")
    print("endnote fixes:", len(notes))
    if not len(notes):
        return xml
    last_chapter = tree.xpath("//h1[@class='chapter_']")[-1]
    notes_section = html.fromstring('<h2 class="section_">Notes</h2>')
    last_chapter.addprevious(notes_section)
    for note in notes:
        note_span = note.xpath("./span[@class='foot_label']")[0]
        note_num = note_span.text
        note_divs = note.xpath(".//div[@class='foot_inner']")
        if not note_divs:
            continue
        note_div = note_divs[0]
        note_div.attrib["class"] = "standard"
        note_id = note_div.xpath(".//*/@id")[0]
        link = html.fromstring('<a href="#{}"><span class="foot_label">{}</span></a>'.format(note_id, note_num))
        note_span.addprevious(link)
        note_div.insert(0, note_span)  # as label for paragraph (move from where it was?)
        last_chapter.addprevious(note_div)
    fixed = html.tostring(tree).decode('utf-8')
    return fixed


def fix_text_direction(xml):
    tree = html.fromstring(xml.encode('utf-8'))
    body_with_dir = tree.xpath("/html/body[@dir='auto']")
    count = len(body_with_dir)
    if not count:
        return xml
    for body in body_with_dir:
        body.attrib["dir"] = "ltr"  # EPUB hates auto text direction, so default to LTR
    print("body dir=auto fixes:", count)
    fixed = html.tostring(tree).decode('utf-8')
    return fixed


def fix(filename, prefix):
    with open(filename, "rb") as bf:
        utf8_in = bf.read()
    text = utf8_in.decode("utf-8")
    fixed = fix_ldquo(text)
    fixed = fix_a_in_blockquote(fixed)
    fixed = fix_div_chapter_label(fixed)
    if html:
        fixed = fix_endnotes(fixed)
        fixed = fix_text_direction(fixed)
    utf8_out = fixed.encode("utf-8")
    with open(prefix + filename, "wb") as bf:
        bf.write(utf8_out)


def main():
    ap = argparse.ArgumentParser()
    add = ap.add_argument
    add("--prefix", default="", help="prefix to give fixed files (else overwrite originals)")
    add("filenames", help="xhtml files to fix", nargs="*")
    args = ap.parse_args()
    for filename in args.filenames:
        fix(filename, args.prefix)

if __name__ == "__main__":
    main()
