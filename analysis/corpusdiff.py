#! /usr/bin/env python3
"""
 corpusdiff [options] -c CORPUS -s SAMPLE

 Given two sets of text files (a base corpus for comparison, and a sample to compare with) produce output
 showing the words in the sample together with their relative frequency in the corpus and in the sample
 (e.g.  WORD X Y, where X is relative corpus frequency and Y is relative sample frequency, intended to produce a
  scatter plot)

 The corpus or sample arguments can be:
   a directory containing .txt files
   a plain text file

 Words that are maybe over-used (assuming the corpus is a fair representation of good style) will appear above the
 diagonal. I don't know if there is such a thing as a word being under-used, but words far under the diagonal may
 indicate a bias in the corpus.

 TODO Possible enhancement options
  -p actually produce a plot using gnuplot or bokeh
  -L: after making the initial distribution of words, attempt to lemmatize them (assuming English text) to
    produce a plot of the root words rather than variants ending in -ing, -ed, -s
"""
import re
import os
import sys
from math import log10
from collections import defaultdict
from argparse import ArgumentParser


def possible_words_iter(text):
    return (m.group(0) for m in re.finditer(r"([a-zA-Z'-]+)", text))


def top_and_tail(text):
    """ remove stuff like Project Gutenberg boilerplate to stop it skewing the word frequencies """
    text = text.strip()
    if text.startswith("Project Gutenberg"):
        try:
            end_first_para = text.index("\nTitle:")
            text = text[end_first_para:]
        except ValueError:
            pass
        try:
            end_of_book = text.index("End of the Project Gutenberg EBook")
            text = text[:end_of_book]
        except ValueError:
            pass
    return text    


def cleanup_punct_iter(words):
    prev = ""
    for word in words:
        if word in ("'s", "'ll", "'d", "n't"):
            word = prev + word  # re-assemble contractions if separated in the corpus
        w = word.strip("'")   # allow apostrophes in words but not at the end (probably quotes)
        w = w.strip("-")    # allow hyphens in words but not at the ends (probably dashes or something)
        prev = w.lower()
        yield prev     # refuse to guess whether case is due to proper nouns or sentence punctuation


def file_texts(path):
    if os.path.isfile(path):
        with open(path) as f:
            yield f.read()
    elif os.path.isdir(path):
        for dirname, subdirs, filenames in os.walk(path):
            for fname in filenames:
                if fname.endswith(".txt"):
                    with open(dirname + "/" + fname) as f:
                        yield f.read()
    else:
        raise ValueError(path)


class CorpusDiffer:
    def __init__(self, minsize=4, stops=()):
        self.corpus_counts = defaultdict(int)
        self.sample_counts = defaultdict(int)
        self.minsize = minsize
        self.stops = set(stops)

    def add_corpus_text(self, text):
        for word in cleanup_punct_iter(possible_words_iter(text)):
            self.corpus_counts[word] += 1

    def add_sample_text(self, text):
        for word in cleanup_punct_iter(possible_words_iter(text)):
            self.sample_counts[word] += 1

    def add_corpus_file(self, path):
        for text in file_texts(path):
            self.add_corpus_text(top_and_tail(text))

    def add_sample_file(self, path):
        for text in file_texts(path):
            self.add_sample_text(top_and_tail(text))

    def frequency_tuples(self):
        corpus_total = sum(self.corpus_counts.values())
        sample_total = sum(self.sample_counts.values())
        for word, count in self.sample_counts.items():
            if len(word) < self.minsize or word in self.stops:
                continue
            ccount = self.corpus_counts[word]
            yield word, ccount/corpus_total, count/sample_total

    def sorted_frequency_tuples(self):
        def badness(wcs):
            """how (relatively) far above the diagonal the word is"""
            _, c, s = wcs
            return s / (c + 0.00001)
        return sorted(self.frequency_tuples(), key=badness, reverse=True)


def scatter_plot(tuples, filename="plot.html", logscale=False):
    """ Make an interactive scatter plot of words, sample frequency vs corpus frequency"""
    try:
        import bokeh
    except ImportError:
        raise NotImplementedError("Need to install bokeh for scatter plot")

    from bokeh.io import output_file, show
    from bokeh.plotting import figure
    from bokeh.models import ColumnDataSource

    output_file(filename)

    # build scatter plot
    if logscale:
        data = [(word, log10(x), log10(y)) for (word, x, y) in tuples if x and y]
    else:
        data = list(tuples)
    word, x, y = zip(*data)
    colours = ["red" if s > c else "grey" for (w, c, s) in data]
    source = ColumnDataSource(data=dict(x=x, y=y, word=word, colour=colours))
    fig = figure(tools="hover, box_zoom, reset", plot_width=1000, plot_height=1000, title="Word Frequency",
                 tooltips=[("word", "@word"), ("F_corpus", "@x"), ("F_sample", "@y")])
    fig.circle('x', 'y', source=source, fill_color='colour', line_color=None)
    fig.xaxis.axis_label = "Relative frequency in corpus"
    fig.yaxis.axis_label = "Relative frequency in sample"

    show(fig)


def main():
    ap = ArgumentParser()
    add = ap.add_argument
    add("--corpus", "-c", nargs="+", help="Corpus file or directory of .txt files")
    add("--sample", "-s", nargs="+", help="Sample file or directory of .txt files")
    add("--plot", "-p", default=None, help="Filename to put bokeh scatter plot in")
    add("--log", "-l", default=False, action="store_true", help="Log scale")

    args = ap.parse_args()
    cd = CorpusDiffer()
    for cf in args.corpus:
        print(cf, file=sys.stderr)
        cd.add_corpus_file(cf)
    for sf in args.sample:
        print(sf, file=sys.stderr)
        cd.add_sample_file(sf)
    print(len(cd.corpus_counts), len(cd.sample_counts), file=sys.stderr)

    if args.plot:
        scatter_plot(cd.frequency_tuples(), filename=args.plot, logscale=args.log)
    else:
        for word, corpus_freq, sample_freq in cd.sorted_frequency_tuples():
            print(word, f"{corpus_freq:.8f}", f"{sample_freq:.8f}")


if __name__ == '__main__':
    main()
