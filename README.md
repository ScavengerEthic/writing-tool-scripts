# README #

### What is this repository for? ###

This is a place to put a few utility scripts I use when writing ebooks in LyX and processing them
with Calibre to convert them to EPUB or MOBI formats.

### How do I get set up? ###

You can install the scripts so they run from the command line with
`pip install setup.py`

### What do these scripts do? ###

* `fixnesting ` _filename_`.xhtml`

  Tidies up some of the possible problems in XHTML produced by
  lyx so it is valid for input to make an EPUB

* `screentones` _input_`.png` _tone1_`.png` _tone2_`.png ...`

  Loads a greyscale PNG and replaces patches of it that are closest
  to the average tone of one of the tone patterns with that pattern.

* `lyx_to_md` _input_`.lyx` _output_`.md`

  Naive conversion of .lyx to markdown syntax. Currently handles
  italic, some headings, and quotes.

### Contribution guidelines ###

Any relevant contributions or suggestions are welcome.

### Who do I talk to? ###

[@ScavengerEthic on twitter](https://twitter.com/ScavengerEthic)