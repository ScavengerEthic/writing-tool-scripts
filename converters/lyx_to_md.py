#! /usr/bin/env python
"""
Takes a lyx file and tries to replace simple lyx formatting commands it understands with
markdown syntax.
"""
from __future__ import print_function
from sys import stderr
from argparse import ArgumentParser
from collections import defaultdict
from itertools import count
import re

MARKDEEP_SCRIPT = (
    '<!-- Markdeep: -->'
    '<style class="fallback">body{visibility:hidden;white-space:pre;font-family:monospace}</style>'
    '<script src="markdeep.min.js"></script>'
    '<script src="https://casual-effects.com/markdeep/latest/markdeep.min.js"></script>'
    '<script>window.alreadyProcessedMarkdeep||(document.body.style.visibility="visible")</script>')

MARKDEEP_UTF8 = '<meta charset="utf-8">'


# noinspection PyMethodMayBeStatic
class LyxToMdConverter:
    def __init__(self, lyxtext: str):
        self.lyxlines = lyxtext.split("\n")
        self.begin_body_index = self.lyxlines.index("\\begin_body")
        self.lyxlines[:self.begin_body_index] = []   # throw away headers we can't interpret yet
        self.mdlines = []
        self.seen_unknown = {"\\begin_body", "\\end_body", "\\end_document", "\\end_inset"}
        self.layout_stack = []
        self.counters = defaultdict(count)
        self.strip_next = False

    def warn(self, text):
        print(text, file=stderr)

    def set_numbering(self, counter, start):
        self.counters[counter] = count(start - 1)

    def convert(self, markdeep=False) -> str:
        if markdeep:
            self.mdlines.append(MARKDEEP_UTF8)
        buffer = ""
        for lineno, line in enumerate(self.lyxlines, 1):
            line = self.character_replacements(line)
            if not line:
                continue
            elif line.startswith("\\"):
                try:
                    buffer = self.accumulate_command(buffer, line)
                except Exception:
                    self.warn("disaster at line {}".format(lineno + self.begin_body_index))
                    raise
            else:
                buffer = self.accumulate_text(buffer, line)
        if buffer:
            self.mdlines.append(buffer)
        if markdeep:
            self.mdlines.append(MARKDEEP_SCRIPT)
        return "\n".join(self.mdlines)

    def character_replacements(self, line):
        line = line.replace("\\SpecialChar ldots", "…")
        line = re.sub(r"(\s|^|—)`(\w)", "\\1\N{left single quotation mark}\\2", line, re.U)
        line = re.sub(r"(\w)'(\s|—|$)", "\\1\N{right single quotation mark}\\2", line, re.U)
        return line

    def accumulate_text(self, buffer, line) -> str:
        spacer = "" if (self.strip_next or line[0] in ",.–'?!") else " "
        self.strip_next = False
        return buffer + spacer + line.strip()

    def accumulate_command(self, buffer, line) -> str:
        tokens = line[1:].strip().split()
        command = tokens[0]
        command_function = getattr(self, "command_" + command, None)
        if command_function:
            return command_function(buffer, tokens[1:])
        else:
            if line not in self.seen_unknown:
                self.warn("unknown command: " + line)
                self.seen_unknown.add(line)
            return buffer   # throw away the command, don't understand it

    def command_begin_layout(self, buffer, args) -> str:
        self.layout_stack.append(args[0])
        return buffer

    def command_end_layout(self, buffer, _) -> str:
        layout = self.layout_stack.pop()
        prefix = {
            "Part": "# Part {} ",
            "Chapter*": "## ",
            "Chapter": "## Chapter {} ",
        }.get(layout, "")
        if "{}" in prefix:
            prefix = prefix.format(next(self.counters[layout]) + 1)   # counters start at 0
        self.mdlines.append(prefix + buffer)
        self.mdlines.append("")
        return ""

    def command_begin_inset(self, buffer, args) -> str:
        if args[0] == "Quotes":
            if args[1] == "eld":
                self.mdlines.append(buffer)
                self.strip_next = True
                buffer = " \N{left double quotation mark}"
            elif args[1] == "els":
                self.mdlines.append(buffer)
                self.strip_next = True
                buffer = " \N{left single quotation mark}"
            elif args[1] == "ers":
                buffer += "\N{right single quotation mark} "
            elif args[1] == "erd":
                buffer += "\N{right double quotation mark} "
            else:
                self.warn("unknown inset {}".format(args))
        elif args[0] == "VSpace":
            self.mdlines.append("")     # shrug
        else:
            self.warn("unknown inset {}".format(args))
        return buffer

    def command_emph(self, buffer, args) -> str:
        if args[0] == "on":
            self.strip_next = True  # no space after underscore
            return buffer + " _"
        else:
            self.strip_next = False  # space after underscore
            return buffer + "_"

    def command_family(self, buffer, _) -> str:
        return buffer


def main():
    ap = ArgumentParser()
    add = ap.add_argument
    add("input", help="path to input .lyx file")
    add("output", help="path to output .md file")
    add("--markdeep", default=False, action="store_true", help="add markdeep script at end")
    add("--chap", type=int, default=1, help="start chapter numbering at this number instead of 1")
    add("--part", type=int, default=1, help="start part numbering at this number instead of 1")
    args = ap.parse_args()
    with open(args.input) as f:
        converter = LyxToMdConverter(f.read())
    with open(args.output, "w") as f:
        converter.set_numbering("Part", args.part)
        converter.set_numbering("Chapter", args.chap)
        f.write(converter.convert(markdeep=args.markdeep))


if __name__ == '__main__':
    main()
